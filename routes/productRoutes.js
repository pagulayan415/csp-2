const express = require('express');

const router = express.Router();

const productController = require('./../controllers/productController')

const auth = require('./../auth');
const user = require('../models/user');


// Get All Products
router.get("/all", (req, res) => {
    productController.getAll()
    .then(resultFromController => res.send(resultFromController));
})

// Get All Active Products
 router.get("/active", auth.verify, (req, res) => {
    productController.getAllActive()
    .then(resultFromController => res.send(resultFromController));
 })

 // Get All Archived Products
 router.get("/archived", (req, res) => {
        productController.getAllArchived()
        .then(resultFromController => res.send(resultFromController));
 })

//  Add Product (Admin Only)
router.post('/add-products', auth.verify, (req, res) =>{
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    // console.log(data.isAdmin);

    if(data.isAdmin == false){
        res.send('The User is not an Admin!')

    }else{
        productController.addProduct(req.body)
        .then(resultFromController => res.send(resultFromController))
    }
})

// Get Specific Product
router.get("/:productId", (req, res) => {

    productController.getProduct(req.params.productId)
    .then(result => res.send(result))
    
})

// Update Product
router.put("/:productId", auth.verify, (req, res) => {
//if isAdmin is equals to false
//	res.send(false)
    
//if isAdmin is equals to true
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    // console.log(data.isAdmin);

    if(data.isAdmin == false){
        res.send('Only Admin can Update a Product!')

    }else{
        productController.updateProduct(req.params.productId, req.body)
        .then(resultFromController => res.send(resultFromController))
    }
})

//Archive Product
//set isActive to False
router.put("/:productId/archive", auth.verify, (req, res) => { 
    //if isAdmin is equals to false
    //	res.send(false)

    //if isAdmin is equals to true
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    // console.log(data.isAdmin);

    if(data.isAdmin == false){
        res.send('Only Admin can Arcive a Product!')

    }else{
        productController.archiveProduct(req.params.productId)
        .then(resultFromController => res.send(resultFromController))
    }
})

// Archive Product
// set isActive to False
router.put("/:productId/activate", auth.verify, (req, res) => { 
    //if isAdmin is equals to false
    //	res.send(false)

    //if isAdmin is equals to true
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    // console.log(data.isAdmin);
    if(data.isAdmin == false){
        res.send('Only Admin can Activate a Product!')

    }else{
        productController.activateProduct(req.params.productId)
        .then(resultFromController => res.send(resultFromController))
    }
})

// Delete a Product (Admin Only)
router.delete("/:productId/delete", auth.verify, (req, res) =>{
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
        productController.deleteProduct(req.params.productId)
        .then(resultFromController => res.send(resultFromController))
    }else{
        res.send('Only Admin can delete a Product!')
    }
})


 module.exports = router