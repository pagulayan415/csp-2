// Import express.js to create routes
const express = require('express')

const router = express.Router();

const userController = require('./../controllers/userControllers');

const auth = require('./../auth')


// User Registration
router.post('/register', (req, res) =>{
    userController.register(req.body)
    .then(resultFromController => res.send(resultFromController))
})

// User Log-in
router.post('/login', (req, res) =>{
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController))
}) 

// Get all Users
router.get('/', auth.verify, (req, res) => {
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
        userController.getAllUsers()
        .then(results => res.send(results))
    }else{
        res.send('Only Admin can view all Accounts!')
    }
})

// Update User details using ID
router.put('/:userId/update', (req, res) =>{
    const userId = req.params.userId;
    userController.updateProfile(userId, req.body)
    .then(result => res.send(result))
})

// Delete User by Id
router.delete('/:userId/delete', auth.verify, (req, res) =>{
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    let userId = req.params.userId;
    if(data.isAdmin == true){
        userController.delete(userId)
        .then((result) => res.send(result))
    }else{
        res.send('Only Admin Account can delete an Account!')
    }
})

// Set User as Admin
router.put('/:userId/set-admin', auth.verify, (req, res) =>{
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
        userController.setAdmin(req.params.userId)
        .then(result => res.send(result))
    }else{
        res.send('Only Admin can set to Regular User to Admin Account!')
    }
})

// Checkout Users Order
router.post("/checkout", auth.verify, (req, res) => {
    let userId = auth.decode(req.headers.authorization)
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
        res.send('The user is an Admin! Cannot Proceed to Checkout!')
    }else{
        userController.checkout(userId, req.body)
        .then(result => res.send(result.latestOrder));
    }
})
        
// View Users Order
router.get("/myOrders", auth.verify, (req, res) => {
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userData: auth.decode(req.headers.authorization)
    }
    if(data.isAdmin == true){
        res.send('Only The User can view his Orders!')
    }else{
        userController.getMyOrders(data.userData)
        .then(resultFromController => res.send(resultFromController));
    }
})

// View All Orders (Admin Only)
router.get("/orders", auth.verify, (req, res) =>{
    let data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == false){
        res.send('Only the Admin can view all the Orders')
    }else{
        userController.getAllOrders()
        .then(resultFromController => res.send(resultFromController));
    }
})






module.exports = router