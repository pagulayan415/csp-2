const jwt = require('jsonwebtoken');
const secret = 'ECONMMERCE';

module.exports.createAccessToken = (user) =>{
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret, {})
}

module.exports.decode = (token) =>{
    // console.log(token)
    // .decode(token, {})
    let slicedToken = token.slice(7, token.length)
    // console.log(slicedToken)
    // console.log (jwt.decode(slicedToken, {complete: true}).payload)
    return jwt.decode(slicedToken, {complete: true}).payload

}

module.exports.verify = (req, res, next) =>{
    // where to get the token? // ans. 'req.headers.authorization'
    let token = req.headers.authorization
    // console.log(token)
    // jwt.verify(token, secret, function)

    if(typeof token !== 'undefined'){
        let slicedToken = token.slice(7, token.length)
        return jwt.verify(slicedToken, secret, (err, data)=>{
            if(err){
                res.send({auth: 'failed'})
            }else{
                next()
            }
        })
    }else{
        res.send(false)
    }
}