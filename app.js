// import experss js
const express = require('express');

const app = express();

// import mongoose
const mongoose = require('mongoose');

let port = 3001;

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

// Connect ot mongoDB database
mongoose.connect('mongodb+srv://Admin:admin123@ecommerce.hod91.mongodb.net/CSP2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Notification if connected to the database
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', () => console.log('Connected to the Database'))


// middleware entry point url (root url before any endpoints)
app.use('/users', userRoutes)
app.use('/products', productRoutes)




app.listen(process.env.PORT || port, () => {
    console.log(`Server is running at port ${port}`)
})