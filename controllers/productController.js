const Product = require('./../models/product')
const auth = require('./../auth');



// Get all Product
module.exports.getAll = () =>{
    return Product.find({})
    .then((result) =>{
        return result;
    })
}

// Retrieve isActive Product
module.exports.getAllActive = () =>{
    return Product.find({isActive: true})
    .then((result, err) =>{
        if(result){
            return result;
        }else{
            return err;
        }
    })
}

// Get all Archived Product
module.exports.getAllArchived = () =>{
    return Product.find({isActive: false})
    .then((result, err) =>{
        if(result){
            return result;
        }else{
            return err;
        }
    })
}

// Add Product (Admin Only)
module.exports.addProduct = (reqBody) =>{
    const {name, description, price} = reqBody
    let newProduct = new Product({
        name: name,
        description: description,
        price: price
    })
    return newProduct.save()
    .then ((result, err) =>{
        if(result){
            return `"Successfully Added Product" \n ${result}`
        }else{
            return err;
        }
    })
}

// Get specific Product by Id
module.exports.getProduct = (reqBody) =>{
    return Product.findById(reqBody)
    .then((result, err) =>{
        if(err){
            return err;
        }else{
            return result;
        }
    })
}

// Update Product (Admin Only)
module.exports.updateProduct = (productId, reqBody) =>{
    // console.log(productId)
    // console.log(reqBody)
    const {name, description, price} =reqBody

    const updatedProduct = {
        name: name,
        description: description,
        price: price
    }
    return Product.findByIdAndUpdate(productId, updatedProduct, {returnDocument: 'after'})
    .then(result =>{
        console.log(result)
        return `"Product Updated Successfully" \n ${result}`;
    })
}

// Archived Product
// Set Product isActive to False (Admin Only)
module.exports.archiveProduct = (reqBody) =>{

    return Product.findById({_id: reqBody})
    .then(result => {
        if(result.isActive == false){
            return `"Product "${result.name}" is already Archived!`
        }else{
            let isNowActive = {
                isActive: false
            }
            return Product.findByIdAndUpdate(reqBody, isNowActive, {returnDocument: 'after'})
            .then((result, err) =>{
                if(err){
                    return err;
                }else{
                    return `Product "${result.name}" is now Archived`
                }
            })  
        }
    })
}

// Unarchived Product
// Set Product is Active to true (Admin Only)
module.exports.activateProduct = (reqBody) =>{
    
    return Product.findById(reqBody)
    .then(result => {
        if(result.isActive == true){
            return `"Product ${result.id} is already Active!"`
        }else{
            let isNowActive = {
                isActive: true
            }
            return Product.findByIdAndUpdate(reqBody, isNowActive, {returnDocument: 'after'})
            .then((result, err) =>{
                if(err){
                    return err;
                }else{
                    return `Product ${reqBody} is now Activated`
                }
            })
        }
    })
}

// Delete a Product (Admin Only)
module.exports.deleteProduct = (productId) =>{
    return Product.findByIdAndDelete(productId)
    .then((result, err) =>{
        if(result){
            return 'Successfully Deleted!';
        }else{
            err
        }
    })
}