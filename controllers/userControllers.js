const User = require('./../models/user');
const auth = require('./../auth.js');
const bcrypt = require('bcrypt');
const product = require('../models/product');
const { json } = require('express/lib/response');
const res = require('express/lib/response');


// Register a user
module.exports.register = (reqBody) => {
    const {email, password} = reqBody

   return  User.findOne({email: email})
    .then(result => {
        if(result !== null && result.email === email){
            return 'Duplcate Email! Please Register a new Email'
        }else{
            let newUser = new User({
                email: email,
                password: bcrypt.hashSync(password, 10)
            })
            return newUser.save()
            .then( registeredUser => {
                registeredUser.password = "********"
                return `"Successfully Registered" \n ${registeredUser}`
            })
        }
    })
}

// User Log-in
module.exports.loginUser = (reqBody) =>{
    const {email, password} = reqBody
    console.log(email)
    return User.findOne({email: email})
    .then((result, err) =>{
        console.log(email)
        if(result == null){
            return 'No Users Found!';
        }else{
            let isPwCorrect = bcrypt.compareSync(password, result.password)
            if(isPwCorrect == true){
                return {access: auth.createAccessToken(result)};
            }else{
                return 'Incorrect Password!';
            }
        }
    })
}

// get all users recores
module.exports.getAllUsers = () => {
    return User.find({})
    .then((result) =>{
        return result;
    })
}

// Update user profile by id
module.exports.updateProfile = (userId, reqBody) =>{
    // console.log(userId)
    // console.log(reqBody)
    const updatedUser = {
        email: reqBody.email,
        password: reqBody.password
    }
    return User.findByIdAndUpdate(userId, updatedUser)
    .then(result =>{
        return `Updated Successfully \n ${result}`
    })
}

// Delete User by Id
module.exports.delete = (userId) =>{
    return User.findByIdAndDelete(userId)
    .then((removedUser, err) =>{
        if(err){
            return false;
        }else{
            return `Successfully Deleted \n ${removedUser}`
        }
    })
}

// Set User as Admin
module.exports.setAdmin = (userId) =>{
    let isNowAdmin = {
        isAdmin: true
    }
    return User.findById(userId)
    .then(result =>{
        if(result.isAdmin == false){
            return User.findByIdAndUpdate(userId, isNowAdmin, {returnDocument: 'after'})
            .then((result, err) =>{
                if(err){
                    return false;
                }else{
                    return `"${result.email}" is now an Admin`;
                }
            })
        }else{
            return `"${result.email}" is already an Admin`;
        }
    })

}

// Check Out Order
module.exports.checkout = async (userData, cart) => {
    //verification
        //save newOrder from user to database
        const newOrder = await User.findByIdAndUpdate(userData.id, { $addToSet: { orders: cart } }, { returnDocument: 'after' })

        //get latest order from user
        const latestOrder = newOrder.orders[newOrder.orders.length - 1]
        const orderId = latestOrder._id

        let productId
        let productDetail
        for (let i = 0; i < cart.products.length; i++) {
            //find Product using product ID from cart.products[0].productId
            productId = cart.products[i].productId
            //save latest order id from user to Product.orders Array
            productDetail = await product.findByIdAndUpdate(productId,
                { $addToSet: { orders: [{ orderId }] } }, { new: true })
        }

        return {
            "productDetails": productDetail,
            "latestOrder": latestOrder
        }

}

// View Users Order
module.exports.getMyOrders = (user) =>{
    return User.findById(user.id)
    .then(result =>{
        const x = []

        for(let i=0; i < result.orders.length; i++){
            x.push({
                "purchasedOn": result.orders[i].purchasedOn,
                "_id": result.orders[i]._id,
                "products": result.orders[i].products,
                "totalAmount": result.orders[i].totalAmount
            })
        }
        return x;
        // console.log(result)
        // let x = []  
        // for(i = 0; i < result.orders.length; i++){
        //     x.push(result.orders[i].purchasedOn)
        //     x.push(result.orders[i]._id)
        //     x.push(result.orders[i].products)
        //     x.push(result.orders[i].totalAmount)
           
        // }
        // return x;
        // return result.orders;

    })
}

// View all Order
module.exports.getAllOrders = async () => {
    // if (userData.isAdmin == true) {

        const allOrders = await User.find().select("-password -isAdmin -__v")
        let allActiveOrders = []

        for (let i = 0; i < allOrders.length; i++) {
            if (allOrders[i].orders.length >= 1) {
                allActiveOrders.push({
                    "_id": allOrders[i]._id,
                    "email": allOrders[i].email,
                    "orders": allOrders[i].orders
                })
            }
        }

        return allActiveOrders

    // } 
    // return { message: "User is not allowed to get all orders" }
}